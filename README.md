env-modules
===========

This is an outdated set of module files used by the [modules](https://github.com/cea-hpc/modules)
package.
